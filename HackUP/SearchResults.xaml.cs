﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Device.Location;
using System.IO;


namespace HackUP
{
    /// <summary>
    /// Interaction logic for SearchResults.xaml
    /// </summary>
    public partial class SearchResults : Window
    {
        public SearchResults()
        {
            InitializeComponent();
        }


        private GeoCoordinateWatcher watcher;
        public double positionX, positionY;


        private void canvas_Loaded(object sender, RoutedEventArgs e)
        {

            watcher = new System.Device.Location.GeoCoordinateWatcher();
            watcher.PositionChanged += watcher_PositionChanged;
            watcher.Start();

        }

        private void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            try
            {
                positionX = e.Position.Location.Latitude;
                positionY = e.Position.Location.Longitude;

                watcher.Stop();


                StreamReader objReader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "html/map.html");
                string line = "";
                line = objReader.ReadToEnd();
                objReader.Close();
                line = line.Replace("[Lat]", 17.ToString());
                line = line.Replace("[Long]", 121.ToString());
                //  StreamWriter page = File.CreateText(AppDomain.CurrentDomain.BaseDirectory +"html/map.html");
                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "html/map.html", line);

                string curDir = Directory.GetCurrentDirectory();
                Uri uri = new Uri(String.Format("file:///{0}/html/map.html", curDir));
                webBrowser.Navigate(uri);
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
