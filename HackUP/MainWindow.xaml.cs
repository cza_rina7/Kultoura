﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.IO;

namespace HackUP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
   
   [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class MainWindow : Window
    {
        public static string content;
        [ComVisible(true)]
        public class ScriptManager
        {
            // Variable to store the form of type Form1.
            private MainWindow mForm;

            // Constructor.
            public ScriptManager(MainWindow form)
            {
                // Save the form so it can be referenced later.
                mForm = form;
            }

            // This method can be called from JavaScript.
            public void MethodToCallFromScript()
            {
                // Call a method on the form.
                mForm.DoSomething();
            }

            // This method can also be called from JavaScript.
            public void yeah(double Lat, double Lng)
            {
                content = Lat.ToString() + ", " + Lng.ToString();
            }
        }
        public void DoSomething()
        {
            
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void textBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            textBox.Clear();
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            Login a = new Login();
            a.Show();
        }
        private void signUp_Click(object sender, RoutedEventArgs e)
        {
            Sign_Up_Form a = new Sign_Up_Form();
            a.Show();
        }
        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void unClicked(object sender, MouseEventArgs e)
        {
            if (textBox.Text.Equals(""))
            {
                textBox.Text = "Search";
            }

        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (textBox.Text.Equals(""))
            {
                textBox.Text = "Search";
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

            
                sea a = new sea();
                a.Show();
            

        
        }
    }
}
