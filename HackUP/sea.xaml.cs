﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HackUP
{
    /// <summary>
    /// Interaction logic for sea.xaml
    /// </summary>
    public partial class sea : Window
    {
        public sea()
        {
            InitializeComponent();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow w = new MainWindow();
            w.Show();
        }

        private void wang_Click(object sender, RoutedEventArgs e)
        {
            SearchResults a = new SearchResults();
            a.Show();
            this.Hide();
        }
    }
}
