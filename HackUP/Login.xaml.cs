﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HackUP
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void username_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            if (password.Text.Equals(""))
            {
                password.Text = "Password";
            }
            username.Clear();
            
        }

        private void password_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (username.Text.Equals(""))
            {
                username.Text = "Username";
            }
            password.Clear();
        }
        private void unClicked(object sender, MouseEventArgs e)
        {
            if (username.Text.Equals(""))
            {
                username.Text = "Username";
            }
            if (password.Text.Equals(""))
            {
               password.Text = "Password";
            }

        }
    }
}
